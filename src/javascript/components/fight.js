import { controls } from '../../constants/controls';

const firstFighterControls = {
  attack: controls.PlayerOneAttack,
  block: controls.PlayerOneBlock,
  fatality: controls.PlayerOneCriticalHitCombination
}

const secondFighterControls = {
  attack: controls.PlayerTwoAttack,
  block: controls.PlayerTwoBlock,
  fatality: controls.PlayerTwoCriticalHitCombination
}

const firstKeysArray = Object.values(firstFighterControls).flat()
const secondKeysArray = Object.values(secondFighterControls).flat()
const validKeys = firstKeysArray.concat(secondKeysArray)

export async function fight(firstFighter, secondFighter) {

  firstFighter.controls = firstFighterControls;
  secondFighter.controls = secondFighterControls;

  firstFighter.totalHealth = firstFighter.health;
  secondFighter.totalHealth = secondFighter.health;

  const longTimeAgo = getNowInSeconds() - 100;

  firstFighter.lastFatalityTime = longTimeAgo;
  secondFighter.lastFatalityTime = longTimeAgo;

  firstFighter.isLeft = true;
  
  let keyPressBuffer = {
    [firstFighter._id]: [],
    [secondFighter._id]: []
  };

  return new Promise((resolve) => {
    document.addEventListener('keydown', e => {
      if (!validKeys.includes(e.code)) {
        return;
      }

      const {attacker, defender} = fightersState(firstFighter, secondFighter, e.code) 

      keyPressBuffer = addToBuffer(keyPressBuffer, attacker, e.code);

      maybeFatality(keyPressBuffer, attacker, defender)
      maybeAttack(attacker, defender, e.code)
      maybeBlock(attacker, e.code)

      updateHealthIndicator(defender)

      if (defender.health <= 0) {
        resolve(attacker)
      }
    })

    document.addEventListener('keyup', e => {
      if (!validKeys.includes(e.code)) {
        return;
      }

      const {attacker} = fightersState(firstFighter, secondFighter, e.code)
      
      keyPressBuffer = removeFromBuffer(keyPressBuffer, attacker, e.code)
      
      maybeUnblock(attacker, e.code)
    })
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  if(blockPower > hitPower) {
    return 0;
  } else {
    return hitPower - blockPower; 
  }
}

export function getFatalityDamage(attacker) {
  return getFatalityHitPower(attacker);
}

export function getHitPower(fighter) {
  // return hit power

  return fighter.attack * chance();
}

export function getFatalityHitPower(fighter) {
  // return fatality hit power

  return fighter.attack * 2;
}

export function getBlockPower(fighter) {
  // return block power

  return fighter.defense * chance();
}

function chance() {
  return Math.random() + 1
}

function fightersState(firstFighter, secondFighter, key) {
  const isFirst = firstKeysArray.includes(key)

  return isFirst ?
    {attacker: firstFighter, defender: secondFighter} :
    {attacker: secondFighter, defender: firstFighter};
}

function addToBuffer(keyPressBuffer, fighter, key) {
  const prevFighterBuffer = keyPressBuffer[fighter._id];
  if (prevFighterBuffer.length < 3) {
    return {
      ...keyPressBuffer,
      [fighter._id]: prevFighterBuffer.concat(key)
    }
  }
  return keyPressBuffer;
}

function removeFromBuffer(keyPressBuffer, fighter, key) {
  const prevFighterBuffer = keyPressBuffer[fighter._id];
  return {
    ...keyPressBuffer,
    [fighter._id]: prevFighterBuffer.filter(prevKey => prevKey != key)
  }
}

function maybeFatality(keyPressBuffer, attacker, defender) {
  const isFatalityKeyPress = 
    keyPressBuffer[attacker._id].join('') === attacker.controls.fatality.join('')

  const isAllowedByLastFatalityTime = 
    (getNowInSeconds() - attacker.lastFatalityTime) > 10
  
  if (isFatalityKeyPress && isAllowedByLastFatalityTime) {
    console.warn('maybeFatality - fatality!')
    
    attacker.lastFatalityTime = getNowInSeconds();

    defender.health -= getFatalityDamage(attacker);
  } else {
    console.warn('maybeFatality - nothing')
  }
}

function maybeAttack(attacker, defender, key) {
  if (defender.isBlocked) {
    console.warn('Cant attack - he blocks');
    return;
  }
  if (attacker.isBlocked) {
    console.warn('Cant attack - I block');
    return;
  }

  if (key === attacker.controls.attack && !defender.isBlocked) {
    const damage = getDamage(attacker, defender);
    defender.health = defender.health - damage;
  }
}

function maybeBlock(attacker, key) {
  if (key === attacker.controls.block) {
    attacker.isBlocked = true;
  }
}

function maybeUnblock(attacker, key) {
  if (key === attacker.controls.block) {
    attacker.isBlocked = false;
  }
}

function updateHealthIndicator(defender) {
  const healthIndicatorId = defender.isLeft ? 
    'left-fighter-indicator' : 
    'right-fighter-indicator';
  const el = document.getElementById(healthIndicatorId);
  const healthPercentage = (defender.health / defender.totalHealth) * 100;
  const healthPercentagePlus = healthPercentage < 0 ? 0 : healthPercentage;
  const newWidth = healthPercentagePlus + "%";
  el.style.width = newWidth;
}

function getNowInSeconds() {
  return new Date().getTime() / 1000;
}
