import {showModal} from './modal'

export function showWinnerModal(fighter) {
  // call showModal function 

  // { title, bodyElement, onClose = () => {} }
  showModal({title: fighter.name, bodyElement: `Congratulations ${fighter.name}!`, onClose: () => {}})
}
